# Workspace

A collection of lua files to search through for all your queries. Make sure to load in all the submodules too by calling `git submodule update --init` from the root of the repository.

### About the submodule

The fa submodule is the FAF repository which contains detailed information about the global functionality available.

### About the other folders

 - `lua`: Contains all the files inside `lua.scd`.
 - `mohodata`: Contains all the files inside `mohodata.scd`.
 - `maps`: Contains all campaign maps from both games.