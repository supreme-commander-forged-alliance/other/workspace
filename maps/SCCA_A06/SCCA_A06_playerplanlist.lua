#****************************************************************************
#**
#**  File     :  /maps/SCCA_A06/SCCA_A06_playerplanlist.lua
#**  Author(s): Drew Staltman
#**
#**  Summary  :
#**
#**  Copyright � 2005 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************
#########################################
# AIBrain Strategic Plans Definitions   #
#########################################

AIPlansList = 
{
    # EARTH Faction Plans
    {   
    },

    # ALIEN Faction Plans
    {   
        '/maps/SCCA_A06/SCCA_A06_playerplan.lua', 
    },

    # RECYCLER Faction Plans
    {   
    },
}
