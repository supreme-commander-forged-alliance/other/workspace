#****************************************************************************
#**
#**  File     :  /maps/SCCA_R05/SCCA_R05_playerplanlist.lua
#**  Author(s): Drew Staltman
#**
#**  Summary  :
#**
#**  Copyright � 2005 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************
#########################################
# AIBrain Strategic Plans Definitions   #
#########################################

AIPlansList = 
{
    # EARTH Faction Plans
    {   
    },

    # ALIEN Faction Plans
    {   
    },

    # RECYCLER Faction Plans
    {   
        '/maps/SCCA_R05/SCCA_R05_playerplan.lua', 
    },
}
