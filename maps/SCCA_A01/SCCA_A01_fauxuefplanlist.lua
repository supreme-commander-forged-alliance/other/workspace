#****************************************************************************
#**
#**  File     :  /maps/SCCA_A01/SCCA_A01_fauxuefplanlist.lua
#**  Author(s): Greg
#**
#**  Summary  :
#**
#**  Copyright � 2006 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************
#########################################
# AIBrain Strategic Plans Definitions   #
#########################################

AIPlansList =
{
    # EARTH Faction Plans
    {
        '/maps/SCCA_A01/SCCA_A01_fauxuefplan.lua',
    },

    # ALIEN Faction Plans
    {
    },

    # RECYCLER Faction Plans
    {
    },
}
