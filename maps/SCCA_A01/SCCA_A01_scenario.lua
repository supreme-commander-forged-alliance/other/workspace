version = 3
ScenarioInfo = {
    name = 'SCCA_A01',
    description = '',
    type = 'campaign',
    starts = true,
    preview = '',
    size = {512, 512},
    map = '/maps/SCCA_A01/SCCA_A01.scmap',
    save = '/maps/SCCA_A01/SCCA_A01_save.lua',
    script = '/maps/SCCA_A01/SCCA_A01_script.lua',
    Configurations = {
        ['standard'] = {
            teams = {
                { name = 'FFA', armies = {'Player','Rhiza','UEF','FauxUEF','FauxRhiza',} },
            },
            customprops = {
            },
        },
    }}
