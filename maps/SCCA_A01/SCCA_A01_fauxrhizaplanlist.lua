#****************************************************************************
#**
#**  File     :  /maps/SCCA_A01/SCCA_A01_fauxrhizaplanlist.lua
#**  Author(s): Greg
#**
#**  Summary  :
#**
#**  Copyright � 2006 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************
#########################################
# AIBrain Strategic Plans Definitions   #
#########################################

AIPlansList =
{
    # EARTH Faction Plans
    {
    },

    # ALIEN Faction Plans
    {
        '/maps/SCCA_A01/SCCA_A01_fauxrhizaplan.lua',
    },

    # RECYCLER Faction Plans
    {
    },
}
