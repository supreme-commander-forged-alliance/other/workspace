#****************************************************************************
#**
#**  File     :  /maps/SCCA_A04/SCCA_A04_playerplanlist.lua
#**  Author(s): Drew Staltman
#**
#**  Summary  :
#**
#**  Copyright � 2005 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************
#########################################
# AIBrain Strategic Plans Definitions   #
#########################################

AIPlansList = 
{
    # EARTH Faction Plans
    {   
    },

    # ALIEN Faction Plans
    {   
        '/maps/SCCA_A04/SCCA_A04_playerplan.lua',
    },

    # RECYCLER Faction Plans
    {   
    },
}
