#****************************************************************************
#**
#**  File     :  /maps/SCCA_E02/SCCA_E02_aeonplanlist.lua
#**  Author(s):
#**
#**  Summary  :
#**
#**  Copyright � 2006 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************
#########################################
# AIBrain Strategic Plans Definitions   #
#########################################

AIPlansList =
{
    # UEF Faction Plans
    {
    },

    # AEON Faction Plans
    {
        '/maps/SCCA_E02/SCCA_E02_aeonaiplan.lua',
    },

    # CYBRAN Faction Plans
    {
    },
}
