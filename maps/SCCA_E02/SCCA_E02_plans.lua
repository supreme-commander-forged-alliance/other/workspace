#****************************************************************************
#**
#**  File     :  /maps/SCCA_E02/SCCA_E02_plans.lua
#**  Author(s):
#**
#**  Summary  :
#**
#**  Copyright � 2005 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************
#########################################
# AIBrain Strategic Plans Definitions   #
#########################################

AIPlansList = 
{
    # EARTH Faction Plans
    {   
        'maps/SCCA_E02/SCCA_E02_playerplan',
    },

    # ALIEN Faction Plans
    {   
        'maps/SCCA_E02/SCCA_E02_aeonaiplan',
    },

    # RECYCLER Faction Plans
    {   
    },
}