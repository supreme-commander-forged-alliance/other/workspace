version = 3
ScenarioInfo = {
    name = 'X1CA_006',
    description = '',
    type = 'campaign',
    starts = true,
    preview = '',
    size = {1024, 1024},
    map = '/maps/X1CA_006/X1CA_006.scmap',
    save = '/maps/X1CA_006/X1CA_006_save.lua',
    script = '/maps/X1CA_006/X1CA_006_script.lua',
    norushradius = 0.000000,
    norushoffsetX_Player = 0.000000,
    norushoffsetY_Player = 0.000000,
    norushoffsetX_Rhiza = 0.000000,
    norushoffsetY_Rhiza = 0.000000,
    norushoffsetX_Fletcher = 0.000000,
    norushoffsetY_Fletcher = 0.000000,
    norushoffsetX_Order = 0.000000,
    norushoffsetY_Order = 0.000000,
    norushoffsetX_Seraphim = 0.000000,
    norushoffsetY_Seraphim = 0.000000,
    norushoffsetX_ControlCenter = 0.000000,
    norushoffsetY_ControlCenter = 0.000000,
    norushoffsetX_OptionZero = 0.000000,
    norushoffsetY_OptionZero = 0.000000,
    Configurations = {
        ['standard'] = {
            teams = {
                { name = 'FFA', armies = {'Player','Rhiza','Fletcher','Order','Seraphim','ControlCenter','OptionZero',} },
            },
            customprops = {
            },
        },
    }}
