Outstanding issues remaining, 8/7/06:

 - (Pri 1) The Aeon Triple nuke attack should fire when the player has built two anti-nuke missiles. (Currently there's no way to test for this.)

 - (Pri 1) There is a bonus objective for firing 8 nukes, but there's currently no trigger to test for this, as far as I know. (I think Drew said he was working on one.)

 - (Pri 1) A motion capture of the Aeon nuke firing should be played during the FireAeonNuke function.

 - (Pri 1) A motion capture of the Cybran LRHAs shooting at the Aeon bases should be played during the StartMission2Part2 function.

 - (Pri 2) The commander and the trucks should have an effect or something when they go through the quantum gate. Also, the commander shouldn't just disappear when he gets to the gate, but should actually get ported or at least look like it.

 - (Pri 3) We may want to add objective updates for how much of the city is still standing. Currently it's hard for the player to gauge how well they're doing on the 'save 90% of the city' secondary objective.

 - (Pri 1) We really need an alert for the player when a research facility is attacked. The single biggest complaint I've gotten from play testers is that they lose facilities without realizing it (this has happened to me as well).

 - (Pri 2) We should investigate giving the quantum gate to the player to use in mission 3. Currently it's hard to tell why they player can't use it, also (it's not clear that it's in a different army). Is it possible to tell the player that the quantum gate network is too dangerous to use right now or something?

 - (Pri 2) For some reason, OSB_HeavyLandAttack_Nuke3Base is only building once. This seems to just happen sometimes.

 - (Pri 1) I have gotten some feedback that this op is harder than the others. I'm not sure how to fix that other than reducing the frequency/size of enemy attacks, and I've played this op too much to be able to gauge how effective or fun that would be. This is definitely something that should be looked into.