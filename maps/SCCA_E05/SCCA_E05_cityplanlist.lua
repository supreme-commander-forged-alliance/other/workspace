#****************************************************************************
#**
#**  File     :  /maps/SCCA_E05/SCCA_E05_cityplanlist.lua
#**  Author(s):  David Tomandl
#**
#**  Summary  :
#**
#**  Copyright � 2005 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************
#########################################
# AIBrain Strategic Plans Definitions   #
#########################################

AIPlansList =
{
    # EARTH Faction Plans
    {
        '/maps/SCCA_E05/SCCA_E05_cityplan.lua',
    },

    # ALIEN Faction Plans
    {
    },

    # RECYCLER Faction Plans
    {
    },
}
