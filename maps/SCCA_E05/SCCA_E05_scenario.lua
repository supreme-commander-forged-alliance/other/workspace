version = 3
ScenarioInfo = {
    name = 'SCCA_E05',
    description = 'Campaign Map: Not Intended for Multiplayer Play',
    type = 'campaign',
    starts = true,
    preview = '',
    size = {1024, 1024},
    map = '/maps/SCCA_E05/SCCA_E05.scmap',
    save = '/maps/SCCA_E05/SCCA_E05_save.lua',
    script = '/maps/SCCA_E05/SCCA_E05_script.lua',
    Configurations = {
        ['standard'] = {
            teams = {
                { name = 'FFA', armies = {'Player','Aeon','City','Cybran',} },
            },
            customprops = {
            },
        },
    }}
