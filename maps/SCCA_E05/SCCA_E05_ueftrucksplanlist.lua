#****************************************************************************
#**
#**  File     :  /maps/SCCA_E05/SCCA_E05_ueftrucksplanlist.lua
#**  Author(s):  David Tomandl
#**
#**  Summary  :
#**
#**  Copyright � 2005 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************
#########################################
# AIBrain Strategic Plans Definitions   #
#########################################

AIPlansList =
{
    # EARTH Faction Plans
    {
        '/maps/SCCA_E05/SCCA_E05_ueftrucksplan.lua',
    },

    # ALIEN Faction Plans
    {
    },

    # RECYCLER Faction Plans
    {
    },
}
