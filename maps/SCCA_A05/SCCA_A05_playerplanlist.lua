#****************************************************************************
#**
#**  File     :  /maps/SCCA_A05/SCCA_A05_playerplanlist.lua
#**  Author(s): Drew Staltman
#**
#**  Summary  :
#**
#**  Copyright � 2005 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************
#########################################
# AIBrain Strategic Plans Definitions   #
#########################################

AIPlansList = 
{
    # EARTH Faction Plans
    {   
    },

    # ALIEN Faction Plans
    {   
        '/maps/SCCA_A05/SCCA_A05_playerplan.lua',
    },

    # RECYCLER Faction Plans
    {   
    },
}
