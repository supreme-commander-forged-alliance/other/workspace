version = 3
ScenarioInfo = {
    name = 'SCCA_A05',
    description = 'Campaign Map: Not Intended for Multiplayer Play',
    type = 'campaign',
    starts = true,
    preview = '',
    size = {1024, 1024},
    map = '/MAPS/SCCA_A05/SCCA_A05.scmap',
    save = '/MAPS/SCCA_A05/SCCA_A05_save.lua',
    script = '/MAPS/SCCA_A05/SCCA_A05_script.lua',
    Configurations = {
        ['standard'] = {
            teams = {
                { name = 'FFA', armies = {'Player','Ariel','UEF','Colonies','FakeUEF',} },
            },
            customprops = {
            },
        },
    }}
