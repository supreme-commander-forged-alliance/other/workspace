version = 3
ScenarioInfo = {
    name = 'X1CA_003',
    description = '',
    type = 'campaign',
    starts = true,
    preview = '',
    size = {2048, 1024},
    map = '/maps/X1CA_003/X1CA_003.scmap',
    save = '/maps/X1CA_003/X1CA_003_save.lua',
    script = '/maps/X1CA_003/X1CA_003_script.lua',
    norushradius = 0.000000,
    norushoffsetX_Player = 0.000000,
    norushoffsetY_Player = 0.000000,
    norushoffsetX_Seraphim = 0.000000,
    norushoffsetY_Seraphim = 0.000000,
    norushoffsetX_Rhiza = 0.000000,
    norushoffsetY_Rhiza = 0.000000,
    norushoffsetX_Princess = 0.000000,
    norushoffsetY_Princess = 0.000000,
    norushoffsetX_Crystals = 0.000000,
    norushoffsetY_Crystals = 0.000000,
    Configurations = {
        ['standard'] = {
            teams = {
                { name = 'FFA', armies = {'Player','Seraphim','Rhiza','Princess','Crystals',} },
            },
            customprops = {
            },
        },
    }}
