#****************************************************************************
#**
#**  File     :  /maps/SCCA_A02/SCCA_A02_playerplanlist.lua
#**  Author(s): Drew Staltman
#**
#**  Summary  :
#**
#**  Copyright � 2005 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************
#########################################
# AIBrain Strategic Plans Definitions   #
#########################################

AIPlansList = 
{
    # EARTH Faction Plans
    {   
    },

    # ALIEN Faction Plans
    {   
        '/maps/SCCA_A02/SCCA_A02_playerplan.lua', 
    },

    # RECYCLER Faction Plans
    {   
    },
}
