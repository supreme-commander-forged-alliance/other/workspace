#****************************************************************************
#**
#**  File     : /maps/X1CA_TUT/X1CA_TUT_script.lua
#**  Author(s): 
#**
#**  Copyright � 2007 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************

local Objectives = import('/lua/ScenarioFramework.lua').Objectives
local ScenarioFramework = import('/lua/ScenarioFramework.lua')
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local Utilities = import('/lua/utilities.lua')
local OpStrings = import('/maps/X1CA_TUT/X1CA_TUT_strings.lua')
local Cinema = import('/lua/cinematics.lua')

# -------
# Globals
# -------
ScenarioInfo.Player = 1

# Army IDs
local Player = ScenarioInfo.Player
local Guen = 2
local Neutrals = 3
local PlayerAlly = 4

# String table
local Strings = {

    TutorialIntro =                         OpStrings.X1T_TU01_010,
    
    PanStart =                              OpStrings.X1T_TU02_010,
    PanTitle =                              OpStrings.X1T_TU02_OBJ_010_010,
    PanDescription =                        OpStrings.X1T_TU02_OBJ_010_020,
    PanComplete =                           OpStrings.X1T_TU02_020,
    PanReminder1 =                          OpStrings.X1T_TU02_050,
    PanReminder2 =                          OpStrings.X1T_TU02_060,
    
    ZoomStart =                             OpStrings.X1T_TU02_030,
    ZoomTitle =                             OpStrings.X1T_TU02_OBJ_010_030,
    ZoomDescription =                       OpStrings.X1T_TU02_OBJ_010_040,
    ZoomComplete =                          OpStrings.X1T_TU02_040,
    ZoomReminder1 =                         OpStrings.X1T_TU02_070,
    ZoomReminder2 =                         OpStrings.X1T_TU02_080,

    MoveACUStart =                          OpStrings.X1T_TU03_010,
    MoveACUTitle =                          OpStrings.X1T_TU03_OBJ_010_010,
    MoveACUDescription =                    OpStrings.X1T_TU03_OBJ_010_020,
    MoveACUComplete =                       OpStrings.X1T_TU03_020,
    MoveACUReminder1 =                      OpStrings.X1T_TU03_030,
    MoveACUReminder2 =                      OpStrings.X1T_TU03_040,
    
    ACUAttackStart =                        OpStrings.X1T_TU03_050,
    ACUAttackTitle =                        OpStrings.X1T_TU03_OBJ_010_030,
    ACUAttackDescription =                  OpStrings.X1T_TU03_OBJ_010_040,
    ACUAttackComplete =                     OpStrings.X1T_TU03_060,
    ACUAttackReminder1 =                    OpStrings.X1T_TU03_070,
    ACUAttackReminder2 =                    OpStrings.X1T_TU03_080, 
    
    EconomyExplanationStart =               OpStrings.X1T_TU04_010,   
    EconomyExplanationContinued =           OpStrings.X1T_TU04_020, 
                       
    
    BuildMassStart =                        OpStrings.X1T_TU04_030,
    BuildMassTitle =                        OpStrings.X1T_TU04_OBJ_010_010,
    BuildMassDescription =                  OpStrings.X1T_TU04_OBJ_010_020,
    BuildMassComplete =                     OpStrings.X1T_TU04_040,
    BuildMassReminder1 =                    OpStrings.X1T_TU04_050,
    BuildMassReminder2 =                    OpStrings.X1T_TU04_060,
    
    BuildPowerStart =                       OpStrings.X1T_TU04_070,
    BuildPowerTitle =                       OpStrings.X1T_TU04_OBJ_010_030,
    BuildPowerDescription =                 OpStrings.X1T_TU04_OBJ_010_040,
    BuildPowerComplete =                    OpStrings.X1T_TU04_080,
    BuildPowerReminder1 =                   OpStrings.X1T_TU04_090,
    BuildPowerReminder2 =                   OpStrings.X1T_TU04_100,    
    
    BuildLandFactoryStart =                 OpStrings.X1T_TU04_110,
    BuildLandFactoryTitle =                 OpStrings.X1T_TU04_OBJ_010_050,
    BuildLandFactoryDescription =           OpStrings.X1T_TU04_OBJ_010_060,
    BuildLandFactoryComplete =              OpStrings.X1T_TU04_120,
    BuildLandFactoryReminder1 =             OpStrings.X1T_TU04_130,
    BuildLandFactoryReminder2 =             OpStrings.X1T_TU04_140,    
    
    BuildArmyStart =                        OpStrings.X1T_TU05_010,
    BuildArmyTitle =                        OpStrings.X1T_TU05_OBJ_010_010,
    BuildArmyDescription =                  OpStrings.X1T_TU05_OBJ_010_020,
    BuildArmyComplete =                     OpStrings.X1T_TU05_020,    
    BuildArmyReminder1 =                    OpStrings.X1T_TU05_030,
    BuildArmyReminder2 =                    OpStrings.X1T_TU05_040,

    BuildControlGroupStart =                OpStrings.X1T_TU05_050,
    BuildControlGroupTitle =                OpStrings.X1T_TU05_OBJ_010_030,
    BuildControlGroupDescription =          OpStrings.X1T_TU05_OBJ_010_040,
    BuildControlGroupComplete =             OpStrings.X1T_TU05_060,
    BuildControlGroupReminder1 =            OpStrings.X1T_TU05_070,
    BuildControlGroupReminder2 =            OpStrings.X1T_TU05_080,                                        

    BuildEngineersStart =                   OpStrings.X1T_TU06_010,
    BuildEngineersTitle =                   OpStrings.X1T_TU06_OBJ_010_010,
    BuildEngineersDescription =             OpStrings.X1T_TU06_OBJ_010_020,
    BuildEngineersComplete =                OpStrings.X1T_TU06_020,
    BuildEngineersReminder1 =               OpStrings.X1T_TU06_030,
    BuildEngineersReminder2 =               OpStrings.X1T_TU06_040,
    
    CaptureStart =                          OpStrings.X1T_TU06_050,
    CaptureTitle =                          OpStrings.X1T_TU06_OBJ_010_030,
    CaptureDescription =                    OpStrings.X1T_TU06_OBJ_010_040,
    CaptureComplete =                       OpStrings.X1T_TU06_060,
    CaptureReminder1 =                      OpStrings.X1T_TU06_070,
    CaptureReminder2 =                      OpStrings.X1T_TU06_080,    
    
    BuildLandDefenseStart =                 OpStrings.X1T_TU06_090,
    BuildLandDefenseTitle =                 OpStrings.X1T_TU06_OBJ_010_050,
    BuildLandDefenseDescription =           OpStrings.X1T_TU06_OBJ_010_060,
    BuildLandDefenseComplete =              OpStrings.X1T_TU06_100,
    BuildLandDefenseReminder1 =             OpStrings.X1T_TU06_110,
    BuildLandDefenseReminder2 =             OpStrings.X1T_TU06_120,
    
    DefendLandStart =                       OpStrings.X1T_TU06_130,
    
    BuildAirDefenseStart =                  OpStrings.X1T_TU06_140,
    BuildAirDefenseTitle =                  OpStrings.X1T_TU06_OBJ_010_070,
    BuildAirDefenseDescription =            OpStrings.X1T_TU06_OBJ_010_080,
    BuildAirDefenseComplete =               OpStrings.X1T_TU06_150,    
    BuildAirDefenseReminder1 =              OpStrings.X1T_TU06_160,
    BuildAirDefenseReminder2 =              OpStrings.X1T_TU06_170,
        
    DefendAirStart =                        OpStrings.X1T_TU06_180,  
    
    BuildRadarStart =                       OpStrings.X1T_TU07_010,
    BuildRadarTitle =                       OpStrings.X1T_TU07_OBJ_010_010,
    BuildRadarDescription =                 OpStrings.X1T_TU07_OBJ_010_020,
    BuildRadarComplete =                    OpStrings.X1T_TU07_020,
    BuildRadarReminder1 =                   OpStrings.X1T_TU07_030,
    BuildRadarReminder2 =                   OpStrings.X1T_TU07_040,
           
    AttackEnemyStart =                      OpStrings.X1T_TU08_010,
    AttackEnemyTitle =                      OpStrings.X1T_TU08_OBJ_010_010,
    AttackEnemyDescription =                OpStrings.X1T_TU08_OBJ_010_020,
    AttackEnemyComplete =                   OpStrings.X1T_TU08_020,
    AttackEnemyReminder1 =                  OpStrings.X1T_TU08_030,
    AttackEnemyReminder2 =                  OpStrings.X1T_TU08_040,
       
    
    EndGameStart =                          OpStrings.X1T_TU09_010,
    EndGameCommander =                      OpStrings.X1T_TU09_030,
    EndGameTitle =                          OpStrings.X1T_TU09_OBJ_010_010,
    EndGameDescription =                    OpStrings.X1T_TU09_OBJ_010_020,
    EndGameComplete =                       OpStrings.X1T_TU09_020,
    EndGameReminder1 =                      OpStrings.X1T_TU09_030,
    EndGameReminder2 =                      OpStrings.X1T_TU09_040,
    
    TransportStart =                        OpStrings.X1T_TU11_010,
    TransportTitle =                        OpStrings.X1T_TU11_OBJ_010_010,
    TransportDescription =                  OpStrings.X1T_TU11_OBJ_010_020,
    TransportComplete =                     OpStrings.X1T_TU11_020,
    TransportReminder1 =                    OpStrings.X1T_TU11_030,
    TransportReminder2 =                    OpStrings.X1T_TU11_040,
    
    TechFactoryStart =                      OpStrings.X1T_TU10_010,
    TechFactoryTitle =                      OpStrings.X1T_TU10_OBJ_010_010,
    TechFactoryDescription =                OpStrings.X1T_TU10_OBJ_010_020,
    TechFactoryComplete =                   OpStrings.X1T_TU10_020,
    TechFactoryReminder1 =                  OpStrings.X1T_TU10_030,
    TechFactoryReminder2 =                  OpStrings.X1T_TU10_040,
       
}

# Area Expansion table
local Areas = {
    M1InitialArea = 'TUT1A1',
    MoveACUTask = 'TUT1A1',
    MoveACUTarget = 'TUT1_MoveACU',
    ACUAttackTask = 'TUT1_AttackACU',
    MassTask = 'TUT1_AttackACU',
    MassTarget = 'TUT1_AttackACU',
    EnergyTask = 'TUT1_AttackACU',
    EnergyTarget = 'TUT1_AttackACU',
    BuildArmyTask = 'TUT1_AttackACU',
    BuildFactoryTask = 'TUT1_Factory',
    EngineerTask = 'TUT1_AttackACU',
    CaptureTask = 'TUT1_Capture',
    LandDefendTask = 'TUT1_LandDefense',
    LandDefendTarget = 'TUT1_PointDefense',
    AirDefendTask = 'TUT1_AirDefense',
    AirDefendTarget = 'TUT1_AntiAir',
    RadarTask = 'TUT1_AttackMinibase',
    RadarTarget = 'TUT1_RadarTarget',
    AttackEnemyTask = 'TUT1_AttackMinibase',    
    TransportTarget = 'TUT1_TransportDestination',
    TransportTask = 'TUT1_TransportArea',
    PanTask = 'TUT1A1',
    ZoomTask = 'TUT1A1',
    Everything = 'TUT1_Everything',
    SpawnTut = 'TUT1_SpawnTut',
    ArmyAttack2 = 'TUT1_SpawnArmy2',
}

# Units IDs
local MassExtractor = categories.ueb1103
local PowerGenerator = categories.ueb1101
local LandFactory = categories.ueb0101
local LandFactoryT2 = categories.ueb0201
local MediumTank = categories.uel0201
local Scout = categories.uel0101
local Engineer = categories.uel0105
local AirFactory = categories.ueb0102
local SeaFactory = categories.ueb0103
local PointDefense = categories.ueb2101
local AntiAir = categories.ueb2104
local Torpedoes = categories.ueb2109
local Wall = categories.ueb5101
local Radar = categories.ueb3101
local AssaultBots = categories.uel0106
local LightArtillery = categories.uel0103
local T3Bot = categories.uel0303

# Timers
local Reminder1Time = 70
local Reminder2Time = 100

# Initial Visible area
local VisArea = 60

# -------
# Other Scenario Functions
# -------
function PlayerLose()
    if(not ScenarioInfo.OpEnded) then
        ScenarioFramework.EndOperationSafety()
        ScenarioInfo.OpComplete = false
# player destroyed
        local unit = ScenarioInfo.PlayerCDR
        ScenarioFramework.CDRDeathNISCamera( unit )
        
    end
end

function PlayerWin()
    ForkThread(function()
        if(not ScenarioInfo.OpEnded) then
            ScenarioFramework.Dialogue(Strings.EndGameComplete, KillGame)
        end
    end)
end

function KillGame()
    ForkThread(
        function()
            WaitSeconds(2)
            ScenarioFramework.EndOperationSafety()
            local endDialog = import('/lua/SimDialogue.lua').Create(LOC("<LOC X1TU_0000>Congratulations, Colonel. Training exercise completed."), {LOC("<LOC _OK>")})
            endDialog.OnButtonPressed = function(self, info)
                ScenarioFramework.ExitGame()
            end
        end
    )
end

# -------
# Startup
# -------
function OnPopulate(scenario)
    ScenarioUtils.InitializeScenarioArmies()
    #Weather.CreateWeather()
end

function OnStart(self)
    # Adjust buildable categories for Player
    ScenarioFramework.AddRestriction(Player, categories.UEF)
    ScenarioFramework.AddRestriction(Player, categories.CYBRAN)

    # Lock off cdr upgrades
    ScenarioFramework.RestrictEnhancements({'ResourceAllocation',
                                            'DamageStablization',
                                            'AdvancedEngineering',
                                            'T3Engineering',
                                            'HeavyAntiMatterCannon',
                                            'LeftPod',
                                            'RightPod',
                                            'Shield',
                                            'ShieldGeneratorField',
                                            'TacticalMissile',
                                            'TacticalNukeMissile',
                                            'Teleporter'})

    for i = 3, table.getn(ArmyBrains) do
        SetIgnorePlayableRect(i, true)
        SetArmyShowScore(i, false)
    end

    ScenarioFramework.SetPlayableArea(Areas.M1InitialArea, false)
    ScenarioFramework.StartOperationJessZoom('TUT1_PanArea', TutorialMissionSetup)
end



function TutorialMissionSetup()
    ScenarioInfo.MissionNumber = 0
    ScenarioFramework.Dialogue(Strings.TutorialIntro, TutorialZoom)
    ScenarioFramework.CreateVisibleAreaLocation( VisArea, ScenarioUtils.MarkerToPosition( 'HomeBaseVisMarker' ), 0, ArmyBrains[Player] )
    # restrict zooming
    ChangeCameraZoom(.5)
end


# ---------
# Pan Mission
# ---------

function TutorialPan()
    ScenarioInfo.MissionNumber = -5
    --Play VO
    ScenarioFramework.Dialogue(Strings.PanStart, TutorialPan_Main)

end


function TutorialPan_Main()
    --Expand playable area when VO is complete
    ScenarioFramework.SetPlayableArea(Areas.PanTask, false)
    
    --Create the objective
    ScenarioInfo.TUTPan = Objectives.Camera(
        'primary',
        'incomplete', 
        Strings.PanTitle, 
        Strings.PanDescription,
        {
            {167, 22, 360},
            {133, 22, 360},
            {150, 22, 346},
            {150, 22, 374},
        }
    )
    
    ScenarioInfo.TUTPan:AddResultCallback(
        function()
            --Congratulating you for moving your commander
            ScenarioFramework.Dialogue(Strings.PanComplete, TutorialZoom)
        end
    )
    
    ScenarioFramework.CreateTimerTrigger(TUTPanReminder1, Reminder1Time) 
end


# ---------
# Zoom Mission
# ---------

function TutorialZoom()
    ScenarioInfo.MissionNumber = -4
    # reenable zooming
    ChangeCameraZoom(1.4)
    --Play VO
    ScenarioFramework.Dialogue(Strings.ZoomStart, TutorialZoom_Main)

end


function TutorialZoom_Main()
    --ExZoomd playable area when VO is complete
    ScenarioFramework.SetPlayableArea(Areas.ZoomTask, false)
    
    --Create the objective
    ScenarioInfo.TUTZoom = Objectives.Camera(
        'primary',
        'incomplete', 
        Strings.ZoomTitle, 
        Strings.ZoomDescription,
        {
            {150, 50, 360},
        }
    )
    
    ScenarioInfo.TUTZoom:AddResultCallback(
        function()
            --Congratulating you for moving your commander
            ScenarioFramework.Dialogue(Strings.ZoomComplete, TutorialMoveACU)
        end
    )
    
    ScenarioFramework.CreateTimerTrigger(TUTZoomReminder1, Reminder1Time)  
end


# ---------
# Move ACU Mission
# ---------

#Move ACU
function TutorialMoveACU()
    ScenarioInfo.MissionNumber = 1
    --Play VO
    ScenarioFramework.Dialogue(Strings.MoveACUStart, TutorialMoveACU_Main)
end


function TutorialMoveACU_Main()
    --Expand playable area when VO is complete
    ScenarioFramework.SetPlayableArea(Areas.MoveACUTask, false)
    
    --Create the objective
    ScenarioInfo.TUTMoveACU = Objectives.CategoriesInArea(
        'primary',
        'incomplete', 
        Strings.MoveACUTitle, 
        Strings.MoveACUDescription,
        'Move',
        {
            MarkUnits = false,
            MarkArea= true,
            ShowFaction = 'UEF',
            Requirements =
            {
                {Area = Areas.MoveACUTarget, Category = categories.COMMAND, CompareOp = '>=', Value = 1, ArmyIndex = Player},
            },
        }
    )
    
    Cinema.EnterNISMode()
    
    Cinema.CameraMoveToMarker('CamStart', 1.5)
    
    ScenarioInfo.PlayerCDR = ScenarioUtils.CreateArmyUnit('Player', 'Commander')
    ScenarioInfo.PlayerCDR:PlayCommanderWarpInEffect()
    ScenarioInfo.PlayerCDR:SetCustomName(LOC '{i CDR_Player}')
    ScenarioFramework.PauseUnitDeath(ScenarioInfo.PlayerCDR)
    ScenarioFramework.CreateUnitDeathTrigger(PlayerLose, ScenarioInfo.PlayerCDR)
    ScenarioInfo.PlayerCDR:SetCanTakeDamage(false)
    ScenarioInfo.PlayerCDR:SetCanBeKilled(false)
    
    WaitSeconds(4)
   
    Cinema.CameraMoveToMarker('CamMoveACU', 2)
    WaitSeconds(1)
    Cinema.CameraMoveToMarker('CamMoveCenter', 1.5)
    
    Cinema.ExitNISMode()
    ScenarioInfo.PlayerCDR:SetCanTakeDamage(false)
    ScenarioInfo.PlayerCDR:SetCanBeKilled(false)
    
    
    ScenarioInfo.TUTMoveACU:AddResultCallback(
        function()
            --Congratulating you for moving your commander
            ScenarioFramework.Dialogue(Strings.MoveACUComplete, TutorialACUAttack)
        end
    )
    
    ScenarioFramework.CreateTimerTrigger(TUTMoveACUReminder1, Reminder1Time)  
end

# ---------
# Attack with ACU Mission
# ---------

#Attack with ACU
function TutorialACUAttack()
    ScenarioInfo.MissionNumber = 2
    -- create fodder for ACU
    ScenarioInfo.FodderUnits = ScenarioUtils.CreateArmyGroup('Guen', 'ACUFodder')
    for i,v in ScenarioInfo.FodderUnits do
        v:SetReclaimable(false)
        v:SetCapturable(false)
    end
    
    --Creation of objective
    ScenarioInfo.TUTACUAttack = Objectives.Kill(
        'primary',
        'incomplete', 
        Strings.ACUAttackTitle, 
        Strings.ACUAttackDescription,
        {
            MarkUnits = true,
            FlashVisible = true,
            Units = ScenarioInfo.FodderUnits,
        }
    )
    
    --VO explaining how to attack with commander.
    ScenarioFramework.Dialogue(Strings.ACUAttackStart, TutorialACUAttack_Main)
end

function TutorialACUAttack_Main()
    --Expand playable area
    ScenarioFramework.SetPlayableArea(Areas.ACUAttackTask, false)
    
    Cinema.EnterNISMode()
    Cinema.CameraMoveToMarker('CamAttackACU', 2)
    WaitSeconds(1.5)
    Cinema.CameraMoveToMarker('CamAttackCenter', 1.5)
    Cinema.ExitNISMode()
    ScenarioInfo.PlayerCDR:SetCanTakeDamage(false)
    ScenarioInfo.PlayerCDR:SetCanBeKilled(false)
    
    ScenarioInfo.TUTACUAttack:AddResultCallback(
        function()
            --Congratulating you for attacking with your commander
            ScenarioFramework.Dialogue(Strings.ACUAttackComplete, TutorialBuildMass)
        end
    )
    
    ScenarioFramework.CreateTimerTrigger(TUTACUAttackReminder1, Reminder1Time)
end

# ---------
# Build Mass Mission
# ---------

function TutorialBuildMass()
    ScenarioInfo.MissionNumber = 3
    ScenarioFramework.Dialogue(Strings.EconomyExplanationStart, nil)
    ScenarioFramework.Dialogue(Strings.EconomyExplanationContinued, nil)
    WaitSeconds(14)
    HighlightUIPanel('economy')
    WaitSeconds(4)

    --VO explaining how awesome mass is, and instructing you to build 4 mass extractors.
    ScenarioFramework.Dialogue(Strings.BuildMassStart, TutorialBuildMass_Main)
end

function TutorialBuildMass_Main()
    --Expand playable area when VO is complete
    ScenarioFramework.SetPlayableArea(Areas.MassTask, false)
    
    Cinema.EnterNISMode()
    Cinema.CameraMoveToMarker('CamMass', 2)
    WaitSeconds(1.5)
    Cinema.CameraMoveToMarker('CamMassCenter', 1.5)
    Cinema.ExitNISMode()
    ScenarioInfo.PlayerCDR:SetCanTakeDamage(false)
    ScenarioInfo.PlayerCDR:SetCanBeKilled(false)
    
    --Create the objective
    ScenarioInfo.TUTBuildMass = Objectives.CategoriesInArea(
        'primary',
        'incomplete', 
        Strings.BuildMassTitle, 
        Strings.BuildMassDescription,
        'Build',
        {
            MarkUnits = false,
            ShowFaction = 'UEF',
            Requirements =
            {
                {Area = Areas.MassTarget, Category = MassExtractor, CompareOp = '>=', Value = 1, ArmyIndex = Player},
                {Area = Areas.MassTarget, Category = MassExtractor, CompareOp = '>=', Value = 2, ArmyIndex = Player},
                {Area = Areas.MassTarget, Category = MassExtractor, CompareOp = '>=', Value = 3, ArmyIndex = Player},                                
            },
        }
    )
    
    ScenarioFramework.RemoveRestriction(Player, MassExtractor)
    ScenarioFramework.PlayUnlockDialogue()
    
    ScenarioInfo.TUTBuildMass:AddResultCallback(
        function()
            --Congratulating you for being awesome enough to build 4 mass extractors.
            ScenarioFramework.Dialogue(Strings.BuildMassComplete, TutorialBuildPower)
        end
    )
    
    ScenarioFramework.CreateTimerTrigger(TUTBuildMassReminder1, Reminder1Time)  
   
end

# ---------
# Build Power Mission
# ---------

function TutorialBuildPower()
    ScenarioInfo.MissionNumber = 4
     --VO explaining how ridiculously abundant energy is, briefly explains adjacency, and then orders you to build power gens. Awesome.
    ScenarioFramework.Dialogue(Strings.BuildPowerStart, TutorialBuildPower_Main)
end

function TutorialBuildPower_Main()
    --Expand playable area when VO is complete
    ScenarioFramework.SetPlayableArea(Areas.EnergyTask, false)

    --Create the objective
    ScenarioInfo.TUTBuildPower = Objectives.CategoriesInArea(
        'primary',
        'incomplete', 
        Strings.BuildPowerTitle, 
        Strings.BuildPowerDescription,
        'Build',
        {
            MarkUnits = false,
            ShowFaction = 'UEF',
            Requirements =
            {
                {Area = Areas.EnergyTarget, Category = PowerGenerator, CompareOp = '>=', Value = 1, ArmyIndex = Player},
                {Area = Areas.EnergyTarget, Category = PowerGenerator, CompareOp = '>=', Value = 2, ArmyIndex = Player},
            },
        }
    )
    
    ScenarioFramework.RemoveRestriction(Player, PowerGenerator)
    ScenarioFramework.PlayUnlockDialogue()
    
    ScenarioInfo.TUTBuildPower:AddResultCallback(
        function()
            --Congratulating you for being awesome enough to build power generators.
            ScenarioFramework.Dialogue(Strings.BuildPowerComplete, TutorialBuildLandFactory)
        end
    )
    
    ScenarioFramework.CreateTimerTrigger(TUTBuildPowerReminder1, Reminder1Time) 
end


# ---------
# Build Factory Mission
# ---------

function TutorialBuildLandFactory()
    ScenarioInfo.MissionNumber = 5
    ScenarioFramework.Dialogue(Strings.BuildLandFactoryStart, TutorialBuildLandFactory_Main)
end


function TutorialBuildLandFactory_Main()
    --Expand playable area when VO is complete
    ScenarioFramework.SetPlayableArea(Areas.EnergyTask, false)
   
    --Create the objective
    ScenarioInfo.TUTBuildLandFactory = Objectives.CategoriesInArea(
        'primary',
        'incomplete', 
        Strings.BuildLandFactoryTitle,          # title
        Strings.BuildLandFactoryDescription,    # description
        'build',                                # action
        {
            MarkUnits = false,
            MarkArea = true,
            ShowFaction = 'UEF',
            Requirements =
            {
                {Area = Areas.BuildFactoryTask, Category = LandFactory, CompareOp = '>=', Value = 1, ArmyIndex = Player},
            },
        }
    )
    
    ScenarioFramework.RemoveRestriction(Player, LandFactory)
    ScenarioFramework.PlayUnlockDialogue()
    
    ScenarioInfo.TUTBuildLandFactory:AddResultCallback(
        function()
            ScenarioFramework.Dialogue(Strings.BuildLandFactoryComplete, TutorialBuildArmy)
        end
    )
    
    ScenarioFramework.CreateTimerTrigger(TUTBuildLandFactoryReminder1, Reminder1Time) 

end


# ---------
# Build Small Army Mission
# ---------

# Build 2 Light Assault Bots
function TutorialBuildArmy()
    ScenarioInfo.MissionNumber = 6                                         
    ScenarioFramework.Dialogue(Strings.BuildArmyStart, TutorialBuildArmy_Main)
end

function TutorialBuildArmy_Main()
    ScenarioInfo.TUTBuildArmy = Objectives.CategoriesInArea(
                    'primary',
                    'incomplete',
                    Strings.BuildArmyTitle,
                    Strings.BuildArmyDescription,
                    'build',
                    {
                        Requirements = {
                           { Area = Areas.BuildArmyTask, Category=AssaultBots, CompareOp='>=', Value=2},
                           { Area = Areas.BuildArmyTask, Category=LightArtillery, CompareOp='>=', Value=2},
                        },
                    }             
    )
    
    ScenarioFramework.RemoveRestriction(Player, AssaultBots + LightArtillery) 
    ScenarioFramework.PlayUnlockDialogue()
    
    ScenarioInfo.TUTBuildArmy:AddResultCallback(
        function()
            ScenarioFramework.Dialogue(Strings.BuildArmyComplete, TutorialBuildControlGroup)
        end
    )
    
    ScenarioFramework.CreateTimerTrigger(TUTBuildArmyReminder1, Reminder1Time)
end



# ---------
# Make a control group
# ---------
function TutorialBuildControlGroup()
    ScenarioInfo.MissionNumber = 7
    # Adjust buildable categories for Player                                        
    ScenarioFramework.Dialogue(Strings.BuildControlGroupStart, TutorialControlGroup_Main)
end

function TutorialControlGroup_Main()
    ScenarioInfo.TUTControlGroup = Objectives.ControlGroup(
                    'primary',
                    'incomplete',
                    Strings.BuildControlGroupTitle,
                    Strings.BuildControlGroupDescription,
                    {
                        Requirements = {
                           { Category=AssaultBots, CompareOp='>=', Value=2},
                           { Category=LightArtillery, CompareOp='>=', Value=2},
                        },
                    }             
    )
    
    ScenarioInfo.TUTControlGroup:AddResultCallback(
        function()
            ScenarioFramework.Dialogue(Strings.BuildControlGroupComplete, TutorialBuildEngineers)
        end
    )
    
    ScenarioFramework.CreateTimerTrigger(TUTBuildControlGroupReminder1, Reminder1Time)
end



# ---------
# Build Engineers Mission
# ---------

# Build 2 Engineers
function TutorialBuildEngineers()
    ScenarioInfo.MissionNumber = 8
    ScenarioFramework.Dialogue(Strings.BuildEngineersStart, TutorialBuildEngineers_Main)
end



function TutorialBuildEngineers_Main()
    ScenarioInfo.TUTBuildEngineers = Objectives.ArmyStatCompare(
        'primary',
        'incomplete',
        Strings.BuildEngineersTitle,
        Strings.BuildEngineersDescription,
        'build',
        {
            Army = 1,
            StatName = 'Units_Active',
            CompareOp = '>=',
            Value = 2,
            Category = categories.ENGINEER * categories.TECH1,
            ShowProgress = true,
        }             
    )
    
    ScenarioFramework.RemoveRestriction(Player, Engineer) #engineer
    ScenarioFramework.PlayUnlockDialogue()
    
    ScenarioInfo.TUTBuildEngineers:AddResultCallback(
        function()
            ScenarioFramework.Dialogue(Strings.BuildEngineersComplete, TutorialCapture)
        end
    )
    
    ScenarioFramework.CreateTimerTrigger(TUTBuildEngineersReminder1, Reminder1Time)
end


# ---------
# Capture a Unit Mission
# ---------

#Capture a PGen
function TutorialCapture()
    ScenarioInfo.MissionNumber = 9
    ScenarioFramework.Dialogue(Strings.CaptureStart, TutorialCapture_Main)
end



function TutorialCapture_Main()
    ScenarioInfo.CaptureUnits = ScenarioUtils.CreateArmyGroup('Neutrals', 'CaptureUnits')
    ScenarioFramework.FlagUnkillable( Neutrals )
    for i,v in ScenarioInfo.CaptureUnits do
        v:SetReclaimable(false)
    end
    
    ScenarioInfo.TUTCapture = Objectives.Capture(
        'primary',
        'incomplete',
        Strings.CaptureTitle,
        Strings.CaptureDescription,
        {
            Units = ScenarioInfo.CaptureUnits,
            NumRequired = 1,
        }             
    )
    
    ScenarioFramework.SetPlayableArea(Areas.CaptureTask, false)
    
    Cinema.EnterNISMode()
    Cinema.CameraMoveToMarker('CamCapture', 2)
    WaitSeconds(1.5)
    Cinema.CameraMoveToMarker('CamCaptureCenter', 1.5)
    Cinema.ExitNISMode()
    ScenarioInfo.PlayerCDR:SetCanTakeDamage(false)
    ScenarioInfo.PlayerCDR:SetCanBeKilled(false)
    
    ScenarioInfo.TUTCapture:AddResultCallback(
        function()
            ScenarioFramework.Dialogue(Strings.CaptureComplete, TutorialMissionBuildLandDefense)
        end
    )
    
    ScenarioFramework.CreateTimerTrigger(TUTCaptureReminder1, Reminder1Time)
end



# ---------
# Build Land Defense Mission
# ---------

function TutorialMissionBuildLandDefense()
    ScenarioInfo.MissionNumber = 10
    --Expand playable area
    ScenarioFramework.SetPlayableArea(Areas.LandDefendTask, false)
    ScenarioFramework.Dialogue(Strings.BuildLandDefenseStart, TutorialMissionBuildLandDefense_Main)    
end



function TutorialMissionBuildLandDefense_Main()
    ScenarioInfo.TUTBuildLandDefense = Objectives.CategoriesInArea(
        'primary',
        'incomplete',
        Strings.BuildLandDefenseTitle,
        Strings.BuildLandDefenseDescription,
        'build',
        {
            MarkArea = true,
            Requirements = {
               { Area = Areas.LandDefendTarget, Category=PointDefense, CompareOp='>=', Value=1},
            },
        }             
    )
    
    ScenarioFramework.RemoveRestriction(Player, PointDefense)
    ScenarioFramework.PlayUnlockDialogue()
    
    Cinema.EnterNISMode()
    Cinema.CameraMoveToMarker('CamBuildLandDefense', 2)
    WaitSeconds(1.5)
    Cinema.CameraMoveToMarker('CamBuildLandDefenseCenter', 1.5)
    Cinema.ExitNISMode()
    ScenarioInfo.PlayerCDR:SetCanTakeDamage(false)
    ScenarioInfo.PlayerCDR:SetCanBeKilled(false)
    
    ScenarioInfo.TUTBuildLandDefense:AddResultCallback(
        function()
            ScenarioFramework.Dialogue(Strings.BuildLandDefenseComplete, TutorialMissionDefendLand)
        end
    )
    
    ScenarioFramework.CreateTimerTrigger(TUTBuildLandDefenseReminder1, Reminder1Time)
end
  

# ---------
# Survive Land Attack Mission
# ---------
function TutorialMissionDefendLand()
    ScenarioInfo.MissionNumber = 11
    --VO warning of imminent attack
    ScenarioFramework.Dialogue(Strings.DefendLandStart, TutorialMissionDefendLand_Main)
end

function TutorialMissionDefendLand_Main()
    ScenarioInfo.TTLandAttack = ForkThread(LandAttackThread)
end

function LandAttackThread()
    --Create units
    local AttackPlatoon = ScenarioUtils.CreateArmyGroupAsPlatoon('Guen', 'AttackLandUnits', 'AttackFormation')
    
    --Assign unit patrol chains
    ScenarioFramework.PlatoonPatrolChain(AttackPlatoon, 'LandAttackChain')
    
    Cinema.EnterNISMode()
    Cinema.CameraMoveToMarker('CamLandAttack', 2)
    WaitSeconds(10)
    Cinema.CameraMoveToMarker('CamLandAttackCenter', 1.5)
    Cinema.ExitNISMode()
    ScenarioInfo.PlayerCDR:SetCanTakeDamage(false)
    ScenarioInfo.PlayerCDR:SetCanBeKilled(false)
    TutorialMissionBuildAirDefense()
end


# ---------
# Build Air Defense Mission
# ---------
function TutorialMissionBuildAirDefense()
    ScenarioInfo.MissionNumber = 12
    --Expand playable area
    ScenarioFramework.SetPlayableArea(Areas.AirDefendTask, false)        
    ScenarioFramework.Dialogue(Strings.BuildAirDefenseStart, TutorialMissionBuildAirDefense_Main)
end

function TutorialMissionBuildAirDefense_Main()
    ScenarioInfo.TUTBuildAirDefense = Objectives.CategoriesInArea(
        'primary',
        'incomplete',
        Strings.BuildAirDefenseTitle,
        Strings.BuildAirDefenseDescription,
        'build',
        {
            MarkArea = true,
            Requirements = {
               { Area = Areas.AirDefendTarget, Category=AntiAir, CompareOp='>=', Value=1},
            },
        }             
    )
    
    ScenarioFramework.RemoveRestriction(Player, AntiAir)
    ScenarioFramework.PlayUnlockDialogue()
    
    Cinema.EnterNISMode()
    Cinema.CameraMoveToMarker('CamBuildAirDefense', 2)
    WaitSeconds(1.5)
    Cinema.CameraMoveToMarker('CamBuildAirDefenseCenter', 1.5)
    Cinema.ExitNISMode()
    ScenarioInfo.PlayerCDR:SetCanTakeDamage(false)
    ScenarioInfo.PlayerCDR:SetCanBeKilled(false)
    
    ScenarioInfo.TUTBuildAirDefense:AddResultCallback(
        function()
            ScenarioFramework.Dialogue(Strings.BuildAirDefenseComplete, TutorialMissionDefendAir)
        end
    )
    
    ScenarioFramework.CreateTimerTrigger(TUTBuildAirDefenseReminder1, Reminder1Time)
end


# ---------
# Survive Air Attack Mission
# ---------

function TutorialMissionDefendAir()
    ScenarioInfo.MissionNumber = 13
    --VO warning of imminent attack
    ScenarioFramework.Dialogue(Strings.DefendAirStart, TutorialMissionDefendAir_Main)
end

function TutorialMissionDefendAir_Main()
    ScenarioInfo.TTAirAttack = ForkThread(AirAttackThread)
end

function AirAttackThread()
    --Create units
    local AttackPlatoon = ScenarioUtils.CreateArmyGroupAsPlatoon('Guen', 'AttackAirUnits', 'AttackFormation')
    
    --Assign unit patrol chains
    ScenarioFramework.PlatoonPatrolChain(AttackPlatoon, 'AirAttackChain')
    Cinema.EnterNISMode()
    Cinema.CameraMoveToMarker('CamAirAttack', 2)
    WaitSeconds(10)
    Cinema.CameraMoveToMarker('CamAirAttackCenter', 1.5)
    Cinema.ExitNISMode()
    ScenarioInfo.PlayerCDR:SetCanTakeDamage(false)
    ScenarioInfo.PlayerCDR:SetCanBeKilled(false)
    #TutorialTechFactory()
    return TutorialBuildRadar()
end

# ---------
# Tech2 Factory Mission
# ---------
#Deprecated
function TutorialTechFactory()
    ScenarioInfo.MissionNumber = 13.5
    ScenarioFramework.Dialogue(Strings.TechFactoryStart, TutorialTechFactory_Main)    
end


function TutorialTechFactory_Main()
    --Expand playable area when VO is complete
    ScenarioInfo.TUTTechFactory = Objectives.ArmyStatCompare(
        'primary',                              # type
        'incomplete',                           # complete
        Strings.TechFactoryTitle,          # title
        Strings.TechFactoryDescription,    # description
        'build',                                # action
        {                                       # target
            Army = Player,
            StatName = 'Units_Active',
            CompareOp = '>=',
            Value = 1,
            Category = LandFactoryT2,
            ShowProgress = true,
        }
    )

    # Adjust buildable categories for Player
    ScenarioFramework.RemoveRestriction(Player, LandFactoryT2)
    ScenarioFramework.PlayUnlockDialogue()
        
    ScenarioInfo.TUTTechFactory:AddResultCallback(
        function()
            ScenarioFramework.RemoveRestriction(Player, categories.uel0208)
            ScenarioFramework.RemoveRestriction(Player, categories.uel0202)
            ScenarioFramework.RemoveRestriction(Player, categories.uel0203)
            ScenarioFramework.PlayUnlockDialogue()
            
            ScenarioFramework.Dialogue(Strings.TechFactoryComplete, TutorialBuildRadar)
        end
    )
    
    ScenarioFramework.CreateTimerTrigger(TUTTechFactoryReminder1, Reminder1Time) 
end

# ---------
# Build Radar Mission
# ---------

function TutorialBuildRadar()
    ScenarioInfo.MissionNumber = 14
    --Expand playable area when VO is complete
    ScenarioFramework.SetPlayableArea(Areas.RadarTask, false)
    ScenarioFramework.Dialogue(Strings.BuildRadarStart, TutorialBuildRadar_Main)
end


function TutorialBuildRadar_Main()
    ScenarioInfo.TUTBuildRadar = Objectives.CategoriesInArea(
                    'primary',
                    'incomplete',
                     Strings.BuildRadarTitle,
                     Strings.BuildRadarDescription,
                    'build',
                    {
                        MarkArea= true,
                        Requirements = {
                           { Area = Areas.RadarTarget, Category=Radar, CompareOp='>=', Value=1},
                        },
                    }             
    )

   
    Cinema.EnterNISMode()
    Cinema.CameraMoveToMarker('CamRadar', 2)
    WaitSeconds(1.5)
    Cinema.CameraMoveToMarker('CamRadarCenter', 1.5)
    Cinema.ExitNISMode()
    ScenarioInfo.PlayerCDR:SetCanTakeDamage(false)
    ScenarioInfo.PlayerCDR:SetCanBeKilled(false)
    
    ScenarioInfo.TUTBuildRadar:AddResultCallback(
        function()
            ScenarioFramework.Dialogue(Strings.BuildRadarComplete, TutorialAttackEnemy)
        end
    )    
    # Adjust buildable categories for Player
    ScenarioFramework.RemoveRestriction(Player, Radar)
    ScenarioFramework.PlayUnlockDialogue()
    
    ScenarioFramework.CreateTimerTrigger(TUTBuildRadarReminder1, Reminder1Time) 
end


# ---------
# Attack Enemy Mission
# ---------

function TutorialAttackEnemy()
    ScenarioInfo.MissionNumber = 15
    --Expand playable area when VO is complete
    ScenarioFramework.SetPlayableArea(Areas.AttackEnemyTask, false)  
    ScenarioFramework.Dialogue(Strings.AttackEnemyStart, TutorialAttackEnemy_Main)
end


function TutorialAttackEnemy_Main()
    ScenarioInfo.FodderUnits = ScenarioUtils.CreateArmyGroup('Guen', 'MiniBase')
    for k,v in ScenarioInfo.FodderUnits do
        v:SetCapturable(false)
        v:SetReclaimable(false)
        v:SetCanTakeDamage(false)
        v:SetCanBeKilled(false)
    end

    --Creation of objective
    ScenarioInfo.TUTAttackEnemy = Objectives.Kill(
        'primary',
        'incomplete', 
        Strings.AttackEnemyTitle, 
        Strings.AttackEnemyDescription,
        {
            MarkUnits = true,
            FlashVisible = true,
            Units = ScenarioInfo.FodderUnits
        }
    )
    
    Cinema.EnterNISMode()
    Cinema.CameraMoveToMarker('CamAttackEnemy', 2.5)
    WaitSeconds(1.5)
    Cinema.CameraMoveToMarker('CamAttackEnemyCenter', 2)
    Cinema.ExitNISMode()
    ScenarioInfo.PlayerCDR:SetCanTakeDamage(false)
    ScenarioInfo.PlayerCDR:SetCanBeKilled(false)

    for k,v in ScenarioInfo.FodderUnits do
        v:SetCanTakeDamage(true)
        v:SetCanBeKilled(true)
    end
    
    
    ScenarioInfo.TUTAttackEnemy:AddResultCallback(
        function()
            ScenarioFramework.Dialogue(Strings.AttackEnemyComplete, TutorialTransport)
        end
    )
    
    ScenarioFramework.CreateTimerTrigger(TUTAttackEnemyReminder1, Reminder1Time) 
end

#-------------------
# Transport Mission
#-------------------
function TutorialTransport()
    ScenarioInfo.MissionNumber = 15.5
    ScenarioFramework.SetPlayableArea(Areas.TransportTask, false)
    ScenarioFramework.Dialogue(Strings.TransportStart, TutorialTransport_Main)
end

function TutorialTransport_Main()
    --Creation of objective
    ScenarioUtils.CreateArmyGroup('Player', 'Transports')
    ScenarioUtils.CreateArmyGroup('Player', 'Reinforcements')
    
    ScenarioInfo.TUTTransport = Objectives.CategoriesInArea(
                    'primary',
                    'incomplete',
                     Strings.TransportTitle,
                     Strings.TransportDescription,
                    'move',
                    {
                        MarkArea= true,
                        Requirements = {
                           { Area = Areas.TransportTarget, Category=T3Bot, CompareOp='>=', Value=1},
                        },
                    }             
    )

    Cinema.EnterNISMode()
    Cinema.CameraMoveToMarker('CamTransportIsland', 3)
    WaitSeconds(3)
    Cinema.CameraMoveToMarker('CamTransportDestination', 2)
    WaitSeconds(2)
    Cinema.ExitNISMode()
    ScenarioInfo.PlayerCDR:SetCanTakeDamage(false)
    ScenarioInfo.PlayerCDR:SetCanBeKilled(false)
    
    
    ScenarioInfo.TUTTransport:AddResultCallback(
        function()
            ScenarioFramework.Dialogue(Strings.TransportComplete, TutorialEndGame)
        end
    )
    
    ScenarioFramework.CreateTimerTrigger(TUTTransportReminder1, Reminder1Time) 
end


# ---------
# End Game Mission
# ---------

function TutorialEndGame()
    ScenarioInfo.MissionNumber = 16

    ScenarioFramework.SetPlayableArea(Areas.Everything, false)
    
    ScenarioInfo.EndGame1 = ScenarioUtils.CreateArmyGroup('Guen', 'EndGameUnits1')
    ScenarioInfo.EndGameBase = ScenarioUtils.CreateArmyGroup('Guen', 'Base')
    for k,v in ScenarioInfo.EndGameBase do
        v:SetCapturable(false)
        v:SetReclaimable(false)
    end
    ScenarioInfo.ExtraPower = ScenarioUtils.CreateArmyGroup('Player', 'ExtraPower')
    ScenarioFramework.Dialogue(Strings.EndGameStart, TutorialEndGame_Main)
end


function TutorialEndGame_Main()
    ScenarioInfo.TT1 = ForkThread(TransportThread)
        
    Cinema.EnterNISMode()
    Cinema.CameraMoveToMarker('CamTransport', 2)
    WaitSeconds(18)
    Cinema.CameraMoveToMarker('CamPostTransport', 1.5)
    Cinema.ExitNISMode()          
    ScenarioInfo.PlayerCDR:SetCanTakeDamage(false)
    ScenarioInfo.PlayerCDR:SetCanBeKilled(false)

    ScenarioFramework.CreateAreaTrigger(SpawnEnemyCDRThread, ScenarioUtils.AreaToRect(Areas.SpawnTut),
        categories.UEF, true, false, ArmyBrains[Player])
        
    ScenarioFramework.CreateAreaTrigger(SpawnArmy2Thread, ScenarioUtils.AreaToRect(Areas.ArmyAttack2),
        categories.UEF, true, false, ArmyBrains[Player])        
end




function TransportThread()
    local reinforcements = ScenarioUtils.CreateArmyGroup('Player', 'TransportReinforcements')
    local transport = ScenarioUtils.CreateArmyGroup('PlayerAlly', 'Transports')[1]
    IssueTransportLoad(reinforcements, transport)
    WaitSeconds(10)
    IssueTransportUnload({transport}, ScenarioUtils.MarkerToPosition('TRANSPORT_LAND_01'))
    WaitSeconds(20)
    IssuePatrol({transport},ScenarioUtils.MarkerToPosition('TRANSPORT_HOME'))
    WaitSeconds(15)     
    ChangeUnitArmy(transport,Player)
end



function SpawnEnemyCDRThread()
    Cinema.EnterNISMode()
    Cinema.CameraMoveToMarker('CamTutSpawn', 2)
    ScenarioInfo.GuenCDR = ScenarioUtils.CreateArmyUnit('Guen', 'Guen')
    ScenarioInfo.GuenCDR:PlayCommanderWarpInEffect()
    
    ScenarioInfo.GuenCDR:SetReclaimable(false)
    ScenarioInfo.GuenCDR:SetCapturable(false)
    
    --Creation of objective
    ScenarioInfo.TUTEndGame = Objectives.Kill(
        'primary',
        'incomplete', 
        Strings.EndGameTitle, 
        Strings.EndGameDescription,
        {
            MarkUnits = true,
            FlashVisible = true,
            Units = {ScenarioInfo.GuenCDR}
        }
    )
    
    WaitSeconds(5)
    Cinema.CameraMoveToMarker('CamTutCenter', 1.5)
    Cinema.ExitNISMode()
    ScenarioInfo.PlayerCDR:SetCanTakeDamage(false)
    ScenarioInfo.PlayerCDR:SetCanBeKilled(false)

    
    ScenarioFramework.Dialogue(Strings.EndGameCommander, nil)    
    
    ScenarioInfo.TUTEndGame:AddResultCallback(PlayerWin)
    
    ScenarioFramework.CreateTimerTrigger(TUTEndGameReminder1, Reminder1Time) 
end

function SpawnArmy2Thread()
    --Create platoon
    local AttackPlatoon = ScenarioUtils.CreateArmyGroupAsPlatoon('Guen', 'EndGameUnits2', 'AttackFormation') 
    
    --Assign unit patrol chains
    ScenarioFramework.PlatoonPatrolChain(AttackPlatoon, 'ArmyAttack2Chain')
    
    --Cinema.EnterNISMode()
    --Cinema.CameraMoveToMarker('CamNewEnemies', 2)
    --WaitSeconds(1.5)
    --Cinema.CameraMoveToMarker('CamNewEnemiesCenter', 1.5)
    --Cinema.ExitNISMode()
    --ScenarioInfo.PlayerCDR:SetCanTakeDamage(false)
    --ScenarioInfo.PlayerCDR:SetCanBeKilled(false)     
    
end

# -------------------
# Objective Reminders
# -------------------
function TUTPanReminder1()
    if(ScenarioInfo.TUTPan.Active) then
        ScenarioFramework.Dialogue(Strings.PanReminder1)
        ScenarioFramework.CreateTimerTrigger(TUTPanReminder2, Reminder2Time)
    end
end

function TUTPanReminder2()
    if(ScenarioInfo.TUTPan.Active) then
        ScenarioFramework.Dialogue(Strings.PanReminder2)
        ScenarioFramework.CreateTimerTrigger(TUTPanReminder1, Reminder2Time)
    end
end


function TUTZoomReminder1()
    if(ScenarioInfo.TUTZoom.Active) then
        ScenarioFramework.Dialogue(Strings.ZoomReminder1)
        ScenarioFramework.CreateTimerTrigger(TUTZoomReminder2, Reminder2Time)
    end
end

function TUTZoomReminder2()
    if(ScenarioInfo.TUTZoom.Active) then
        ScenarioFramework.Dialogue(Strings.ZoomReminder2)
        ScenarioFramework.CreateTimerTrigger(TUTZoomReminder1, Reminder2Time)
    end
end


function TUTMoveACUReminder1()
    if(ScenarioInfo.TUTMoveACU.Active) then
        ScenarioFramework.Dialogue(Strings.MoveACUReminder1)
        ScenarioFramework.CreateTimerTrigger(TUTMoveACUReminder2, Reminder2Time)
    end
end

function TUTMoveACUReminder2()
    if(ScenarioInfo.TUTMoveACU.Active) then
        ScenarioFramework.Dialogue(Strings.MoveACUReminder2)
        ScenarioFramework.CreateTimerTrigger(TUTMoveACUReminder1, Reminder2Time)
    end
end


function TUTACUAttackReminder1()
    if(ScenarioInfo.TUTACUAttack.Active) then
        ScenarioFramework.Dialogue(Strings.ACUAttackReminder1)
        ScenarioFramework.CreateTimerTrigger(TUTACUAttackReminder2, Reminder2Time)
    end
end

function TUTACUAttackReminder2()
    if(ScenarioInfo.TUTACUAttack.Active) then
        ScenarioFramework.Dialogue(Strings.ACUAttackReminder2)
        ScenarioFramework.CreateTimerTrigger(TUTACUAttackReminder1, Reminder2Time)
    end
end

function TUTBuildMassReminder1()
    if(ScenarioInfo.TUTBuildMass.Active) then
        ScenarioFramework.Dialogue(Strings.BuildMassReminder1)
        ScenarioFramework.CreateTimerTrigger(TUTBuildMassReminder2, Reminder2Time)
    end
end

function TUTBuildMassReminder2()
    if(ScenarioInfo.TUTBuildMass.Active) then
        ScenarioFramework.Dialogue(Strings.BuildMassReminder2)
        ScenarioFramework.CreateTimerTrigger(TUTBuildMassReminder1, Reminder2Time)
    end
end

function TUTBuildPowerReminder1()
    if(ScenarioInfo.TUTBuildPower.Active) then
        ScenarioFramework.Dialogue(Strings.BuildPowerReminder1)
        ScenarioFramework.CreateTimerTrigger(TUTBuildPowerReminder2, Reminder2Time)
    end
end

function TUTBuildPowerReminder2()
    if(ScenarioInfo.TUTBuildPower.Active) then
        ScenarioFramework.Dialogue(Strings.BuildPowerReminder2)
        ScenarioFramework.CreateTimerTrigger(TUTBuildPowerReminder1, Reminder2Time)
    end
end

function TUTBuildLandFactoryReminder1()
    if(ScenarioInfo.TUTBuildLandFactory.Active) then
        ScenarioFramework.Dialogue(Strings.BuildLandFactoryReminder1)
        ScenarioFramework.CreateTimerTrigger(TUTBuildLandFactoryReminder2, Reminder2Time)
    end
end

function TUTBuildLandFactoryReminder2()
    if(ScenarioInfo.TUTBuildLandFactory.Active) then
        ScenarioFramework.Dialogue(Strings.BuildLandFactoryReminder2)
        ScenarioFramework.CreateTimerTrigger(TUTBuildLandFactoryReminder1, Reminder2Time)
    end
end

function TUTBuildArmyReminder1()
    if(ScenarioInfo.TUTBuildArmy.Active) then
        ScenarioFramework.Dialogue(Strings.BuildArmyReminder1)
        ScenarioFramework.CreateTimerTrigger(TUTBuildArmyReminder2, Reminder2Time)
    end
end

function TUTBuildArmyReminder2()
    if(ScenarioInfo.TUTBuildArmy.Active) then
        ScenarioFramework.Dialogue(Strings.BuildArmyReminder2)
        ScenarioFramework.CreateTimerTrigger(TUTBuildArmyReminder1, Reminder2Time)
    end
end


function TUTBuildControlGroupReminder1()
    if(ScenarioInfo.TUTControlGroup.Active) then
        ScenarioFramework.Dialogue(Strings.BuildControlGroupReminder1)
        ScenarioFramework.CreateTimerTrigger(TUTBuildControlGroupReminder2, Reminder2Time)
    end
end

function TUTBuildControlGroupReminder2()
    if(ScenarioInfo.TUTControlGroup.Active) then
        ScenarioFramework.Dialogue(Strings.BuildControlGroupReminder2)
        ScenarioFramework.CreateTimerTrigger(TUTBuildControlGroupReminder1, Reminder2Time)
    end
end

function TUTBuildEngineersReminder1()
    if(ScenarioInfo.TUTBuildEngineers.Active) then
        ScenarioFramework.Dialogue(Strings.BuildEngineersReminder1)
        ScenarioFramework.CreateTimerTrigger(TUTBuildEngineersReminder2, Reminder2Time)
    end
end

function TUTBuildEngineersReminder2()
    if(ScenarioInfo.TUTBuildEngineers.Active) then
        ScenarioFramework.Dialogue(Strings.BuildEngineersReminder2)
        ScenarioFramework.CreateTimerTrigger(TUTBuildEngineersReminder1, Reminder2Time)
    end
end


function TUTCaptureReminder1()
    if(ScenarioInfo.TUTCapture.Active) then
        ScenarioFramework.Dialogue(Strings.CaptureReminder1)
        ScenarioFramework.CreateTimerTrigger(TUTCaptureReminder2, Reminder2Time)
    end
end

function TUTCaptureReminder2()
    if(ScenarioInfo.TUTCapture.Active) then
        ScenarioFramework.Dialogue(Strings.CaptureReminder2)
        ScenarioFramework.CreateTimerTrigger(TUTCaptureReminder1, Reminder2Time)
    end
end

function TUTBuildLandDefenseReminder1()
    if(ScenarioInfo.TUTBuildLandDefense.Active) then
        ScenarioFramework.Dialogue(Strings.BuildLandDefenseReminder1)
        ScenarioFramework.CreateTimerTrigger(TUTBuildLandDefenseReminder2, Reminder2Time)
    end
end

function TUTBuildLandDefenseReminder2()
    if(ScenarioInfo.TUTBuildLandDefense.Active) then
        ScenarioFramework.Dialogue(Strings.BuildLandDefenseReminder2)
        ScenarioFramework.CreateTimerTrigger(TUTBuildLandDefenseReminder1, Reminder2Time)
    end
end

function TUTBuildAirDefenseReminder1()
    if(ScenarioInfo.TUTBuildAirDefense.Active) then
        ScenarioFramework.Dialogue(Strings.BuildAirDefenseReminder1)
        ScenarioFramework.CreateTimerTrigger(TUTBuildAirDefenseReminder2, Reminder2Time)
    end
end

function TUTBuildAirDefenseReminder2()
    if(ScenarioInfo.TUTBuildAirDefense.Active) then
        ScenarioFramework.Dialogue(Strings.BuildAirDefenseReminder2)
        ScenarioFramework.CreateTimerTrigger(TUTBuildAirDefenseReminder1, Reminder2Time)
    end
end

function TUTBuildRadarReminder1()
    if(ScenarioInfo.TUTBuildRadar.Active) then
        ScenarioFramework.Dialogue(Strings.BuildRadarReminder1)
        ScenarioFramework.CreateTimerTrigger(TUTBuildRadarReminder2, Reminder2Time)
    end
end

function TUTBuildRadarReminder2()
    if(ScenarioInfo.TUTBuildRadar.Active) then
        ScenarioFramework.Dialogue(Strings.BuildRadarReminder2)
        ScenarioFramework.CreateTimerTrigger(TUTBuildRadarReminder1, Reminder2Time)
    end
end

function TUTAttackEnemyReminder1()
    if(ScenarioInfo.TUTAttackEnemy.Active) then
        ScenarioFramework.Dialogue(Strings.AttackEnemyReminder1)
        ScenarioFramework.CreateTimerTrigger(TUTAttackEnemyReminder2, Reminder2Time)
    end
end

function TUTAttackEnemyReminder2()
    if(ScenarioInfo.TUTAttackEnemy.Active) then
        ScenarioFramework.Dialogue(Strings.AttackEnemyReminder2)
        ScenarioFramework.CreateTimerTrigger(TUTAttackEnemyReminder1, Reminder2Time)
    end
end

function TUTTransportReminder1()
    if(ScenarioInfo.TUTTransport.Active) then
        ScenarioFramework.Dialogue(Strings.TransportReminder1)
        ScenarioFramework.CreateTimerTrigger(TUTTransportReminder2, Reminder2Time)
    end
end

function TUTTransportReminder2()
    if(ScenarioInfo.TUTTransport.Active) then
        ScenarioFramework.Dialogue(Strings.TransportReminder2)
        ScenarioFramework.CreateTimerTrigger(TUTTransportReminder1, Reminder2Time)
    end
end

function TUTEndGameReminder1()
    if(ScenarioInfo.TUTEndGame.Active) then
        ScenarioFramework.Dialogue(Strings.EndGameReminder1)
        ScenarioFramework.CreateTimerTrigger(TUTEndGameReminder2, Reminder2Time)
    end
end

function TUTEndGameReminder2()
    if(ScenarioInfo.TUTEndGame.Active) then
        ScenarioFramework.Dialogue(Strings.EndGameReminder2)
        ScenarioFramework.CreateTimerTrigger(TUTEndGameReminder1, Reminder2Time)
    end
end

function TUTTechFactoryReminder1()
    if(ScenarioInfo.TUTTechFactory.Active) then
        ScenarioFramework.Dialogue(Strings.TechFactoryReminder1)
        ScenarioFramework.CreateTimerTrigger(TUTTechFactoryReminder2, Reminder2Time)
    end
end

function TUTTechFactoryReminder2()
    if(ScenarioInfo.TUTTechFactory.Active) then
        ScenarioFramework.Dialogue(Strings.TechFactoryReminder2)
        ScenarioFramework.CreateTimerTrigger(TUTTechFactoryReminder1, Reminder2Time)
    end
end

# ---------
# Keyboard functions
# ---------


#function OnCtrlAltF5()
#    KillGame()
#end



