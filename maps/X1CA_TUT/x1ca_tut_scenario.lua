version = 3
ScenarioInfo = {
    name = 'X1CA_TUT',
    description = '',
    type = 'campaign',
    starts = true,
    preview = '',
    size = {512, 512},
    map = '/maps/SCMP_019/SCMP_019.scmap',
    save = '/maps/X1CA_TUT/X1CA_TUT_save.lua',
    script = '/maps/X1CA_TUT/X1CA_TUT_script.lua',
    norushradius = 0.000000,
    norushoffsetX_Player = 0.000000,
    norushoffsetY_Player = 0.000000,
    norushoffsetX_Guen = 0.000000,
    norushoffsetY_Guen = 0.000000,
    norushoffsetX_Neutrals = 0.000000,
    norushoffsetY_Neutrals = 0.000000,
    norushoffsetX_PlayerAlly = 0.000000,
    norushoffsetY_PlayerAlly = 0.000000,
    Configurations = {
        ['standard'] = {
            teams = {
                { name = 'FFA', armies = {'Player','Guen','Neutrals','PlayerAlly',} },
            },
            customprops = {
            },
        },
    }}
