#****************************************************************************
#**
#**  File     :  /maps/SCCA_R01/SCCA_R01_aeonplanlist.lua
#**  Author(s): Greg
#**
#**  Summary  :
#**
#**  Copyright � 2006 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************
#########################################
# AIBrain Strategic Plans Definitions   #
#########################################

AIPlansList =
{
    # EARTH Faction Plans
    {
    },

    # ALIEN Faction Plans
    {
        '/maps/SCCA_R01/SCCA_R01_aeonplan.lua',
    },

    # RECYCLER Faction Plans
    {
    },
}
