version = 3
ScenarioInfo = {
    name = 'MAP_SC_OpC1',
    description = '',
    type = 'campaign',
    starts = true,
    preview = '',
    size = {512, 512},
    map = '/maps/SCCA_R01/SCCA_R01.scmap',
    save = '/maps/SCCA_R01/SCCA_R01_save.lua',
    script = '/maps/SCCA_R01/SCCA_R01_script.lua',
    Configurations = {
        ['standard'] = {
            teams = {
                { name = 'FFA', armies = {'Player','Aeon','UEF','Symbiont','Dostya','FauxUEF','Wreckage_Holding',} },
            },
            customprops = {
            },
        },
    }}
