#****************************************************************************
#**
#**  File     :  /maps/SCCA_Tutorial/SCCA_Tutorial_cybran2planlist.lua
#**  Author(s): Marc Scattergood
#**
#**  Summary  : AIBrain Strategic Plans Definitions
#**
#**  Copyright � 2006 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************

AIPlansList = 
{
    # ---
    # UEF
    # ---
    {   
    },

    # ----
    # Aeon
    # ----
    {   
    },

    # ------
    # Cybran
    # ------
    {   
        '/maps/scca_tutorial/scca_tutorial_cybranplan.lua',
    },
}