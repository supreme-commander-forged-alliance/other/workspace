#****************************************************************************
#**
#**  File     :  /maps/SCCA_Tutorial/SCCA_Tutorial_playerplanlist.lua
#**  Author(s):  Marc Scattergood
#**
#**  Summary  : AIBrain Strategic Plans Definitions
#**
#**  Copyright � 2006 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************

AIPlansList = 
{
    # ---
    # UEF
    # ---
    {   
        '/maps/SCCA_Tutorial/SCCA_Tutorial_playerplan.lua',
    },

    # ----
    # Aeon
    # ----
    {   
    },

    # ------
    # Cybran
    # ------
    {
    },
}