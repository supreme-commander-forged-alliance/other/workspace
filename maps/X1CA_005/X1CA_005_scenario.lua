version = 3
ScenarioInfo = {
    name = 'X1CA_005',
    description = '',
    type = 'campaign',
    starts = true,
    preview = '',
    size = {1024, 1024},
    map = '/maps/X1CA_005/X1CA_005.scmap',
    save = '/maps/X1CA_005/X1CA_005_save.lua',
    script = '/maps/X1CA_005/X1CA_005_script.lua',
    norushradius = 0.000000,
    norushoffsetX_Player = 0.000000,
    norushoffsetY_Player = 0.000000,
    norushoffsetX_Fletcher = 0.000000,
    norushoffsetY_Fletcher = 0.000000,
    norushoffsetX_Hex5 = 0.000000,
    norushoffsetY_Hex5 = 0.000000,
    norushoffsetX_QAI = 0.000000,
    norushoffsetY_QAI = 0.000000,
    norushoffsetX_Aeon = 0.000000,
    norushoffsetY_Aeon = 0.000000,
    norushoffsetX_UEF = 0.000000,
    norushoffsetY_UEF = 0.000000,
    norushoffsetX_Order = 0.000000,
    norushoffsetY_Order = 0.000000,
    norushoffsetX_Brackman = 0.000000,
    norushoffsetY_Brackman = 0.000000,
    Configurations = {
        ['standard'] = {
            teams = {
                { name = 'FFA', armies = {'Player','Fletcher','Hex5','QAI','Aeon','UEF','Order','Brackman',} },
            },
            customprops = {
            },
        },
    }}
