version = 3
ScenarioInfo = {
    name = 'X1CA_002',
    description = '',
    type = 'campaign',
    starts = true,
    preview = '',
    size = {1024, 1024},
    map = '/maps/X1CA_002/X1CA_002.scmap',
    save = '/maps/X1CA_002/X1CA_002_save.lua',
    script = '/maps/X1CA_002/X1CA_002_script.lua',
    norushradius = 0.000000,
    norushoffsetX_Player = 0.000000,
    norushoffsetY_Player = 0.000000,
    norushoffsetX_Order = 0.000000,
    norushoffsetY_Order = 0.000000,
    norushoffsetX_QAI = 0.000000,
    norushoffsetY_QAI = 0.000000,
    norushoffsetX_Loyalist = 0.000000,
    norushoffsetY_Loyalist = 0.000000,
    norushoffsetX_OrderNeutral = 0.000000,
    norushoffsetY_OrderNeutral = 0.000000,
    Configurations = {
        ['standard'] = {
            teams = {
                { name = 'FFA', armies = {'Player','Order','QAI','Loyalist','OrderNeutral',} },
            },
            customprops = {
            },
        },
    }}
