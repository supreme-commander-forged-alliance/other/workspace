version = 3
ScenarioInfo = {
    name = 'SCCA_E01',
    description = '',
    type = 'campaign',
    starts = true,
    preview = '',
    size = {512, 512},
    map = '/maps/SCCA_E01/SCCA_E01.scmap',
    save = '/maps/SCCA_E01/SCCA_E01_save.lua',
    script = '/maps/SCCA_E01/SCCA_E01_script.lua',
    Configurations = {
        ['standard'] = {
            teams = {
                { name = 'FFA', armies = {'Player','Arnold','Cybran','EastResearch',} },
            },
            customprops = {
            },
        },
    }}
