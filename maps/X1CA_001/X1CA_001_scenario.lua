version = 3
ScenarioInfo = {
    name = 'X1CA_001',
    description = '',
    type = 'campaign',
    starts = true,
    preview = '',
    size = {1024, 1024},
    map = '/maps/X1CA_001/X1CA_001.scmap',
    save = '/maps/X1CA_001/X1CA_001_save.lua',
    script = '/maps/X1CA_001/X1CA_001_script.lua',
    norushradius = 0.000000,
    norushoffsetX_Player = 0.000000,
    norushoffsetY_Player = 0.000000,
    norushoffsetX_Seraphim = 0.000000,
    norushoffsetY_Seraphim = 0.000000,
    norushoffsetX_Order = 0.000000,
    norushoffsetY_Order = 0.000000,
    norushoffsetX_UEF = 0.000000,
    norushoffsetY_UEF = 0.000000,
    norushoffsetX_Civilians = 0.000000,
    norushoffsetY_Civilians = 0.000000,
    Configurations = {
        ['standard'] = {
            teams = {
                { name = 'FFA', armies = {'Player','Seraphim','Order','UEF','Civilians',} },
            },
            customprops = {
            },
        },
    }}
