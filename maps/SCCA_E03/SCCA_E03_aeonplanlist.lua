#****************************************************************************
#**
#**  File     :  /maps/SCCA_E03/SCCA_E03_aeonplanlist.lua
#**  Author(s): Drew Staltman
#**
#**  Summary  :
#**
#**  Copyright � 2005 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************
#########################################
# AIBrain Strategic Plans Definitions   #
#########################################

AIPlansList =
{
    # EARTH Faction Plans
    {
    },

    # ALIEN Faction Plans
    {
        '/maps/SCCA_E03/SCCA_E03_aeonplan_one.lua',
    },

    # RECYCLER Faction Plans
    {
    },
}
