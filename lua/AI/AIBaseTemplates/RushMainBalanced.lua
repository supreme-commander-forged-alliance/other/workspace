#***************************************************************************
#*
#**  File     :  /lua/ai/AIBaseTemplates/RushMainBalanced.lua
#**
#**  Summary  : Manage engineers for a location
#**
#**  Copyright � 2005 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************

BaseBuilderTemplate {
    BaseTemplateName = 'RushMainBalanced',
    Builders = {
        # ==== ECONOMY ==== #
        # Factory upgrades
        'T1BalancedUpgradeBuilders',
        'T2BalancedUpgradeBuilders',
        
        # Engineer Builders
        'EngineerFactoryBuilders',
        'T1EngineerBuilders',
        'T2EngineerBuilders',
        'T3EngineerBuilders',
        'EngineerFactoryConstruction Balance',
        'EngineerFactoryConstruction',
        
        # Engineer Support buildings
        'EngineeringSupportBuilder',
        
        # Build energy at this base
        'EngineerEnergyBuilders',
        
        # Build Mass high pri at this base
        'EngineerMassBuildersHighPri',
        
        # Extractors
        'Time Exempt Extractor Upgrades',
        
        # ACU Builders
        'Balanced Rush Initial ACU Builders',
        'ACUBuilders',
        'ACUUpgrades',
        
        # ACU Defense
        'T1ACUDefenses',
        'T2ACUDefenses',
        'T2ACUShields',
        'T3ACUShields',
        'T3ACUNukeDefenses',
        
        # ==== EXPANSION ==== #
        'EngineerExpansionBuildersFull',
        'EngineerExpansionBuildersSmall',
        
        # ==== DEFENSES ==== #
        'T2MissileDefenses',
        'T3NukeDefenses',
        'T3NukeDefenseBehaviors',
        
        # ==== NAVAL EXPANSION ==== #
        'NavalExpansionBuilders',
        
        # ==== LAND UNIT BUILDERS ==== #
        'T1LandFactoryBuilders',
        'T2LandFactoryBuilders',
        'T3LandFactoryBuilders',
        
        'FrequentLandAttackFormBuilders',
        'MassHunterLandFormBuilders',
        'MiscLandFormBuilders',
        'UnitCapLandAttackFormBuilders',
        
        'T1LandAA',
        'T2LandAA',

        'T1ReactionDF',
        'T2ReactionDF',
        'T3ReactionDF',
        
        'T2Shields',
        'ShieldUpgrades',
        'T3Shields',

        # ==== AIR UNIT BUILDERS ==== #
        'T1AirFactoryBuilders',
        'T2AirFactoryBuilders',
        'T3AirFactoryBuilders',
        'FrequentAirAttackFormBuilders',
        'MassHunterAirFormBuilders',
        
        'UnitCapAirAttackFormBuilders',
        'ACUHunterAirFormBuilders',
        
        'TransportFactoryBuilders',
        
        'T1AntiAirBuilders',
        'T2AntiAirBuilders',
        'T3AntiAirBuilders',
        'BaseGuardAirFormBuilders',

        # ==== EXPERIMENTALS ==== #
        'MobileLandExperimentalEngineers',
        'MobileLandExperimentalForm',
        
        'MobileAirExperimentalEngineers',
        'MobileAirExperimentalForm',
    },
    NonCheatBuilders = {
        'AirScoutFactoryBuilders',
        'AirScoutFormBuilders',
        
        'LandScoutFactoryBuilders',
        'LandScoutFormBuilders',
        
        'RadarEngineerBuilders',
        'RadarUpgradeBuildersMain',
        
        'CounterIntelBuilders',
    },
    BaseSettings = {
        EngineerCount = {
            Tech1 = 15,
            Tech2 = 10,
            Tech3 = 10,
            SCU = 3,
        },
        FactoryCount = {
            Land = 6,
            Air = 4,
            Sea = 0,
            Gate = 1,
        },
        MassToFactoryValues = {
            T1Value = 6,
            T2Value = 15,
            T3Value = 22.5
        },
    },
    ExpansionFunction = function(aiBrain, location, markerType)
        return 0
    end,
    FirstBaseFunction = function(aiBrain)
        local per = ScenarioInfo.ArmySetup[aiBrain.Name].AIPersonality
        if not per then 
            return 1, 'rushbalanced'
        end
        
        if per == 'random' then
            return Random(1,100), 'rushbalanced'

        elseif per != 'rush' and per != 'adaptive' and per != '' then
            return 1, 'rushbalanced'
        end

        local mapSizeX, mapSizeZ = GetMapSize()
        local isIsland = false
        
        local startX, startZ = aiBrain:GetArmyStartPos()
        local islandMarker = import('/lua/AI/AIUtilities.lua').AIGetClosestMarkerLocation(aiBrain, 'Island', startX, startZ)
        if islandMarker then
            isIsland = true
        end
        
        if true then
            #return 1000, 'rushbalanced'
        end
        
        #If we're playing on an island map, do not use this plan often
        if isIsland and mapSizeX > 1024 and mapSizeZ > 1024 then
            return Random(25, 50), 'rushbalanced'

        elseif mapSizeX < 512 and mapSizeZ < 512 then
            return Random(98, 100), 'rushbalanced'

        elseif mapSizeX >= 512 and mapSizeZ >= 512 and mapSizeX <= 1024 and mapSizeZ <= 1024 then
            return Random(50, 100), 'rushbalanced'

        else
            return Random(25, 75), 'rushbalanced'
        end
    end,
}