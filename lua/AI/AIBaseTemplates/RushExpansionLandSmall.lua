#***************************************************************************
#*
#**  File     :  /lua/ai/AIBaseTemplates/RushExpansionLandSmall.lua
#**
#**  Summary  : Manage engineers for a location
#**
#**  Copyright � 2005 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************

BaseBuilderTemplate {
    BaseTemplateName = 'RushExpansionLandSmall',
    Builders = {
        # ==== ECONOMY ==== #
        # Factory upgrades
        'T1BalancedUpgradeBuildersExpansion',
        'T2BalancedUpgradeBuildersExpansion',
        
        # Engineer Builders
        'EngineerFactoryBuilders',
        'T1EngineerBuilders',
        'T2EngineerBuilders',
        'T3EngineerBuilders',
        'EngineerFactoryConstruction',
        'LandInitialFactoryConstruction',
        
        # Build Mass low pri at this base
        'EngineerMassBuildersLowerPri',
        
        # ==== LAND UNIT BUILDERS ==== #
        'T1LandFactoryBuilders',
        'T2LandFactoryBuilders',
        'T3LandFactoryBuilders',
        'FearlessFrequentLandAttackFormBuilders',
        'MassHunterLandFormBuilders',
        'MiscLandFormBuilders',

        'T1ReactionDF',
        'T2ReactionDF',
        'T3ReactionDF',
		},
    NonCheatBuilders = {        
        'LandScoutFactoryBuilders',
        'LandScoutFormBuilders',
        
        'RadarEngineerBuilders',
        'RadarUpgradeBuildersExpansion',
    },
    BaseSettings = {
        EngineerCount = {
            Tech1 = 8,
            Tech2 = 8,
            Tech3 = 8,
            SCU = 0,
        },
        
        FactoryCount = {
            Land = 2,
            Air = 0,
            Sea = 0,
            Gate = 0,
        },
        
        MassToFactoryValues = {
            T1Value = 12,
            T2Value = 30,
            T3Value = 40,
        },
        NoGuards = true,
    },
    ExpansionFunction = function(aiBrain, location, markerType)
        if markerType != 'Expansion Area' then
            return 0
        end
        
        local personality = ScenarioInfo.ArmySetup[aiBrain.Name].AIPersonality
        if not( personality == 'adaptive' or personality == 'rushland' ) then
            return 0
        end

        local threatCutoff = 10 # value of overall threat that determines where enemy bases are
        local distance = import('/lua/ai/AIUtilities.lua').GetThreatDistance( aiBrain, location, threatCutoff )
        if not distance or distance > 1000 then
            return 10
        elseif distance > 500 then
            return 25
        elseif distance > 250 then
            return 50
        else
            return 100
        end
        
        return 0
    end,
}