#***************************************************************************
#*
#**  File     :  /lua/ai/AIBaseTemplates/RushMainAir.lua
#**
#**  Summary  : Manage engineers for a location
#**
#**  Copyright � 2005 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************

BaseBuilderTemplate {
    BaseTemplateName = 'RushMainAir',
    Builders = {
        # ==== ECONOMY ==== #
        # Factory upgrades
        'T1BalancedUpgradeBuilders',
        'T2BalancedUpgradeBuilders',
        
        # Engineer Builders
        'EngineerFactoryBuilders',
        'T1EngineerBuilders',
        'T2EngineerBuilders',
        'T3EngineerBuilders',
        'EngineerFactoryConstruction',
        'EngineerFactoryConstructionAirHigherPriority',
        
        # Engineer Support buildings
        'EngineeringSupportBuilder',
        
        # Build energy at this base
        'EngineerEnergyBuilders',
        
        # Build Mass high pri at this base
        'EngineerMassBuildersHighPri',
        
        # Extractors
        'Time Exempt Extractor Upgrades',
        
        # ACU Builders
        'Air Rush Initial ACU Builders',
        'ACUBuilders',
        'ACUUpgrades',
        
        # ACU Defense
        'T1ACUDefenses',
        'T2ACUDefenses',
        'T2ACUShields',
        'T3ACUShields',
        'T3ACUNukeDefenses',
        
        # ==== EXPANSION ==== #
        'EngineerExpansionBuildersFull',
        'EngineerExpansionBuildersSmall',
        
        # ==== DEFENSES ==== #
        'T2MissileDefenses',
        'T3NukeDefenses',
        'T3NukeDefenseBehaviors',
        
        'MiscDefensesEngineerBuilders',
        
        # ==== NAVAL EXPANSION ==== #
        'NavalExpansionBuilders',
        
        # ==== LAND UNIT BUILDERS ==== #
        'T1LandFactoryBuilders',
        'T2LandFactoryBuilders',
        'T3LandFactoryBuilders',
        
        'FrequentLandAttackFormBuilders',
        'MassHunterLandFormBuilders',
        'MiscLandFormBuilders',
        'UnitCapLandAttackFormBuilders',
        
        'T1LandAA',
        'T2LandAA',

        'T1ReactionDF',
        'T2ReactionDF',
        'T3ReactionDF',

        'T2Shields',
        'ShieldUpgrades',
        'T3Shields',
        
        # ==== AIR UNIT BUILDERS ==== #
        'T1AirFactoryBuilders',
        'T2AirFactoryBuilders',
        'T3AirFactoryBuilders',
        'BigAirAttackFormBuilders',
        'MassHunterAirFormBuilders',
        'UnitCapAirAttackFormBuilders',
       
        'ACUHunterAirFormBuilders',
        
        'TransportFactoryBuilders',
        
        #'T1AntiAirBuilders',
        'T2AntiAirBuilders',
        'T3AntiAirBuilders',
        'BaseGuardAirFormBuilders',

        # ==== EXPERIMENTALS ==== #        
        'MobileAirExperimentalEngineers',
        'MobileAirExperimentalForm',
        
        'NukeBuildersEngineerBuilders',
        'NukeFormBuilders',
    },
    NonCheatBuilders = {
        'AirScoutFactoryBuilders',
        'AirScoutFormBuilders',
        
        'LandScoutFactoryBuilders',
        'LandScoutFormBuilders',
        
        'RadarEngineerBuilders',
        'RadarUpgradeBuildersMain',
        
        'CounterIntelBuilders',
    },
    BaseSettings = {
        EngineerCount = {
            Tech1 = 15,
            Tech2 = 10,
            Tech3 = 10,
            SCU = 1,
        },
        FactoryCount = {
            Land = 2,
            Air = 10,
            Sea = 0,
            Gate = 1,
        },
        MassToFactoryValues = {
            T1Value = 6,
            T2Value = 15,
            T3Value = 22.5
        },
    },
    ExpansionFunction = function(aiBrain, location, markerType)
        return 0
    end,
    FirstBaseFunction = function(aiBrain)
        local per = ScenarioInfo.ArmySetup[aiBrain.Name].AIPersonality
        if not per then 
            return 1, 'rushair'
        end
        
        if per == 'random' then
            return Random(1,100), 'rushair'
        
        elseif per != 'rush'and per != 'adaptive' and per != '' then
            return 1, 'rushair'
        end
        
        if true then
            #return 1000, 'rushair'
        end

        local mapSizeX, mapSizeZ = GetMapSize()
        local startX, startZ = aiBrain:GetArmyStartPos()
        
        if mapSizeX < 512 and mapSizeZ < 512 then
            return 25, 'rushair'
            
        elseif mapSizeX <= 512 and mapSizeZ <= 512 then
            return Random(25, 75), 'rushair'

        elseif mapSizeX <= 1024 and mapSizeZ < 1024 then
            return Random(60, 100), 'rushair'
            
        else
            return Random(80, 100), 'rushair'
        end
    end,
}