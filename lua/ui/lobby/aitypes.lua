--*****************************************************************************
--* File: lua/modules/ui/lobby/aitypes.lua
--* Author: Chris Blackwell
--* Summary: Contains a list of AI types and names for the game
--*
--* Copyright � 2006 Gas Powered Games, Inc.  All rights reserved.
--*****************************************************************************

aitypes = {
    {
        key = 'easy',
        name = "<LOC lobui_0347>AI: Easy",
    },
    {
        key = 'medium',
        name = "<LOC lobui_0349>AI: Normal",
    },
    {
        key = 'adaptive',
        name = "<LOC lobui_0368>AI: Adaptive",
    },
    {
        key = 'rush',
        name = "<LOC lobui_0360>AI: Rush",
    },
    {
        key = 'turtle',
        name = "<LOC lobui_0372>AI: Turtle",
    },
    {
        key = 'tech',
        name = "<LOC lobui_0370>AI: Tech",
    },
    {
        key = 'random',
        name = "<LOC lobui_0374>AI: Random",
    },
    {
        key = 'adaptivecheat',
        name = "<LOC lobui_0379>AIx: Adaptive",
    },
    {
        key = 'rushcheat',
        name = "<LOC lobui_0380>AIx: Rush",
    },
    {
        key = 'turtlecheat',
        name = "<LOC lobui_0384>AIx: Turtle",
    },
    {
        key = 'techcheat',
        name = "<LOC lobui_0385>AIx: Tech",
    },
    {
        key = 'randomcheat',
        name = "<LOC lobui_0395>AIx: Random",
    },
}
