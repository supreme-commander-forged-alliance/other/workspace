GameColors = {
    PlayerColors = {
	"FFe80a0a",
	"DarkGreen",
	"FF131cd3",
	"Goldenrod",
 	"FFA7A7A7",
 	"Darkslategray",
	"FF202020",
	"FF4d0505",
	"FF2E8B57",
	"BlueViolet",

    },

    ArmyColors = {
	"FFe80a0a",
	"DarkGreen",
	"FF131cd3",
	"Goldenrod",
 	"FFA7A7A7",
 	"Darkslategray",
	"FF202020",
	"FF4d0505",
	"FF2E8B57",
	"BlueViolet",
 
    },

    CivilianArmyColor = "BurlyWood",

    TeamColorMode = {
        Self = "RoyalBlue",
        Enemy = "FFE80A0A",
        Ally = "DarkGreen",
        Neutral = "Goldenrod",
    },

    UnidentifiedColor = "FF808080",
}

