#****************************************************************************
#**  File     :  /lua/sim/Projectile.lua
#**  Summary  : The Projectile lua module
#**
#**  Copyright � 2008 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************
local Entity = import('/lua/sim/Entity.lua').Entity

Projectile = Class(moho.projectile_methods, Entity) {

    # Do not call the base class __init and __post_init, we already have a c++ object
    __init = function(self,spec)
    end,

    __post_init = function(self,spec)
    end,
}
