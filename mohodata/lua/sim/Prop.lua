#****************************************************************************
#**  File     :  /lua/sim/Prop.lua
#**  Summary  : The Prop lua module
#**
#**  Copyright � 2008 Gas Powered Games, Inc.  All rights reserved.
#****************************************************************************
local Entity = import('/lua/sim/Entity.lua').Entity

Prop = Class(moho.prop_methods, Entity) {
}
